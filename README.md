# Site

The skyclan site

## Why ruby?
I wanted to learn ruby, and even though coding it in python would be easier (due to easier communication with skybot) I thought it would be a fun experience.

To prove the power of the ruby language, ~~I made this entire webserver using only standard libraries~~. I found the implementation of certain libraries to be slightly difficult, so the gems I used are listed below.

1. fiber_scheduler
2. sqlite3 (not needed for now)

# How to install?
Simply `git clone https://codeberg.org/SkyClan/Site`, and then `ruby main.rb` with the next argument being the port of your choice.

Be sure to forward the ports if you would like this to be accessed outside of your LAN.

### Contributors
1. Blazerunner
2. Darkfang
3. Spikestar
require 'socket'
require 'timeout'
Dir["lib/*.rb"].each { |file| require_relative file }

$server = TCPServer.new ARGV[0] || 80
$server.setsockopt Socket::IPPROTO_TCP, Socket::TCP_NODELAY, 1
$server.listen Socket::SOMAXCONN
Fiber.set_scheduler  Scheduler.new

loop do 
  Fiber.schedule do
    conn = $server.accept
    time = Time.new
     
    req = Request.new readall conn
    res = Response.new 
    res.headers["Content-Type"] = "text/html; charset=utf-8"
      
    case req.path
    when '/'
      res.content_file = "index.html"
    when '/about'
      res.content_file = "about.html"
    when '/history'
      res.content_file = "history.html"
    when '/members'
      res.content_file = "members.html"
    when '/skybucks'
      res.content_file = "skybucks.html"
    when '/style.css'
      res.headers["Content-Type"] = "text/css"
      res.content_file = "style.css"
    when '/main.js'
      res.headers["Content-Type"] = 'text/javascript'
      res.content_file = "main.js"
    when '/ws'
      if req.headers["Sec-Websocket-Key"] && req.method.upcase == "GET"
        res.code = 101
        res.headers["Upgrade"] = "websocket"
        res.headers["Connection"] = "Upgrade"
        res.headers["Sec-Websocket-Accept"] = websocket_key req.headers["Sec-Websocket-Key"]
      else 
        res.code = 400
      end
    else
      res.code = 404
      res.content_file = "404.html"
    end
      
    conn.write res
  
    puts "#{conn.remote_address.inspect_sockaddr} - - [#{time}] \"#{req.req_line}\" #{res.code} #{res.to_s.length}" 
    
    unless res.code == 101
      conn.close
    else
      #websocket thingy for reading
      # while im stil changing to fibers, bear with me lol
      while msg = recv(conn)
        puts msg
      end
    end
  end
end

trap "INT" do 
  $server.shutdown 
end

Thread.current.scheduler.run

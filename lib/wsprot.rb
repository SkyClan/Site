require 'digest/sha1'

UNIV_IDEN = "258EAFA5-E914-47DA-95CA-C5AB0DC85B11"

def websocket_key client_str
  Digest::SHA1.base64digest "#{client_str}#{UNIV_IDEN}" 
end

def recv socket
  fin_opcode = socket.read(1).bytes[0]
  raise "Message not complete" unless fin_opcode & 0x80
  
  mask_and_len = socket.read(1).bytes[0]
  raise "Message is unmasked" unless mask_and_len & 0x80
  
  case len = mask_and_len & 0x7f
  when 126
    len = socket.read(2).unpack('n')[0]
  when 127
    len = socket.read(8).unpack('Q>')[0]
  end
  
  keys = socket.read(4).bytes
  decoded = socket.read(len).bytes.each_with_index.map { |byte, index| byte ^ keys[index % 4] }  
  decoded.pack('c*')
end

def encode msg
  encoded = [129]
  encoded +=   
    if (size = msg.bytesize) <= 125
      [size]
    elsif size < 2**16
      [126] + [size].pack("n").bytes
    else
      [127] + [size].pack("Q>").bytes
    end
  (encoded + msg.bytes).pack "C*"
end
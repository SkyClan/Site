class Request
  attr_reader :method, :path, :protocol, :req_line, :headers, :raw_headers, :content, :raw
  
  def initialize raw
    @raw = raw
    unless @raw == ""
      @req_line = @raw.split("\r\n", 2)[0]
      @method, @path, @protocol = @req_line.split " "
      @raw_headers, @content = @raw[(@req_line.length + 2)..-1].split  "\r\n" * 2 
      @headers = {}

      @raw_headers.split("\r\n").each do |head| 
        key, value = head.split ": ", 2
        @headers[key] = value
      end
    end 
  end

  def to_s 
    @raw
  end
end
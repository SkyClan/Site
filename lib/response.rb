require 'yaml'

CODE_RES = YAML.load_file "conf/codes.yaml"
FOLDER = YAML.load_file "conf/files.yaml"

class Response
  attr_accessor :code, :protocol, :headers, :content, :cookies
  
  def initialize(code: 200, protocol: "HTTP/1.1", headers: {}, content: nil, cookies: {})
    @code = code
    @protocol = protocol
    @headers = headers
    @content = content
    @cookies = cookies
  end

  def to_s
    if @content
      @headers["Content-Length"] = @content.length unless @headers["Content-Length"]
    end
    "#{@protocol} #{@code} #{CODE_RES[@code]}\r\n#{format_head}\r\n#{@content}"
  end

  def format_head
    formatted = ""
    @headers.each do |key, value|
      formatted+= "#{key}: #{value}\r\n"
    end
    @cookies.each do |key, value|
      formatted+= "Set-Cookie: #{key}=#{value}\r\n"
    end
    formatted
  end

  def content_file= file
    unless file.include?("/") && !(file.include?('.')) 
      @content = File.read("#{FOLDER[file.split('.')[1]]}/#{file}") 
    else
      @content = File.read(file)
    end
  end
      
end

require 'socket'

ENDING = "\r\n\r\n"

def readall conn
  request = ''
  begin
    loop do
      request << conn.read_nonblock(1024 * 16)
      break if check? request
    end
  rescue Errno::EAGAIN
    IO.select [conn]
    retry
  rescue EOFError
  end
  request
end

private 

def check? data
  (data.end_with?(ENDING) && data !~ /.+Content-Length:\s\d+\r\n.+/) || data.split(ENDING)[1].length == data.match(/(?<=Content-Length:\s)\d+(?=\r\n)/)[1]
end